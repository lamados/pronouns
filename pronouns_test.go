package pronouns

import (
	"testing"
)

func TestSet_String(t *testing.T) {
	for _, set := range All {
		t.Log(`"` + set.String() + `"`)
	}
}

func TestSetCollection_String(t *testing.T) {
	collection := SetCollection{TheyThem, SheHer} // hello
	t.Log(`"` + collection.String() + `"`)
}

func BenchmarkSet_String(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_ = TheyThem.String()
	}
}
