package pronouns

import (
	"strings"
)

// Set is a set of third-person pronouns.
type Set struct {
	Subject     string `json:"subject"`
	Object      string `json:"object"`
	Dependent   string `json:"dependent"`
	Independent string `json:"independent"`
	Reflexive   string `json:"reflexive"`

	Plural bool `json:"plural"`
}

// SetCollection is a collection of pronoun sets.
type SetCollection []Set

// Binary pronoun sets.
var (
	HeHim  = Set{"he", "him", "his", "his", "himself", false}
	SheHer = Set{"she", "her", "her", "hers", "herself", false}
)

// Normative-ish pronoun sets.
var (
	TheyThem = Set{"they", "them", "their", "theirs", "themselves", true}
	ItIts    = Set{"it", "it", "its", "its", "itself", false}
	OneOnes  = Set{"one", "one", "one's", "one's", "oneself", false}
)

// Common neopronoun sets.
var (
	AeAer   = Set{"ae", "aer", "aer", "aers", "aerself", false}
	EyEm    = Set{"ey", "em", "eir", "eirs", "emself", true}
	FaeFaer = Set{"fae", "faer", "faer", "faers", "faerself", false}
	XeXem   = Set{"xe", "xem", "xyr", "xyrs", "xemself", false}
	ZeHir   = Set{"ze", "hir", "hir", "hirs", "hirself", false}
	ZeZir   = Set{"ze", "zir", "zir", "zirs", "zirself", false}
)

// TODO: other neopronouns? https://en.pronouns.page/pronouns

// Pronoun set collections.
var (
	Binary       = []Set{HeHim, SheHer}
	Normativeish = []Set{TheyThem, ItIts, OneOnes}
	Neopronouns  = []Set{AeAer, EyEm, FaeFaer, XeXem, ZeHir, ZeZir}

	All = append(append(Binary, Normativeish...), Neopronouns...)
)

func (set Set) String() string {
	b := strings.Builder{}
	b.WriteString(set.Subject)
	b.WriteByte('/')
	if set.Subject == set.Object {
		b.WriteString(set.Dependent)
	} else {
		b.WriteString(set.Object)
	}
	return b.String()
}

func (collection SetCollection) String() string {
	if len(collection) == 1 {
		return collection[0].String()
	}
	b := strings.Builder{}
	for i, set := range collection {
		b.WriteString(set.Subject)
		if i != len(collection)-1 {
			b.WriteByte('/')
		}
	}
	return b.String()
}
